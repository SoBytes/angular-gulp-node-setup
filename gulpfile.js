var gulp = require('gulp')
var jshint = require('gulp-jshint');
var sass = require('gulp-ruby-sass')
var connect = require('gulp-connect')
var browserify = require('browserify')
var minify = require('gulp-minify');
var source = require('vinyl-source-stream')

var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var less = require('gulp-less'); 
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var env = process.env.GULP_ENV;

gulp.task('connect', function () {
    connect.server({
        root: 'public',
        port: 4000,
        livereload: true,
        fallback: 'public/index.html'
    })
})

gulp.task('lint', function() {
  return gulp.src('public/js/*.js')
    .pipe(jshint())
    //.pipe(jshint.reporter('YOUR_REPORTER_HERE'));
});

gulp.task('browserify', function() {
    return browserify('./app/app.js')
        .bundle()
        .pipe(source('main.js'))
        .pipe(gulp.dest('./public/js/'));
})

gulp.task('compress', function() {
  gulp.src('public/js/*.js')
    .pipe(minify())
    .pipe(gulp.dest('public/js/')) 
})

gulp.task('js', function () {
    return gulp.src(['bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'public/js/*.js'])
        .pipe(concat('bootstrap.js'))
        .pipe(gulpif(env === 'prod', uglify()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/js'));
});
 
gulp.task('css', function () {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css',
        './public/less/**/*.less'])
        .pipe(gulpif(/[.]less/, less()))
        .pipe(concat('bootstrap.css'))
        .pipe(gulpif(env === 'prod', uglifycss()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/css'));
});
 
gulp.task('img', function() {
    return gulp.src('./public/img/**/*.*')
        .pipe(gulp.dest('public/img'));
});

gulp.task('sass', function() {
    return sass('./sass/style.sass')
        .pipe(gulp.dest('./public/css'))
})

gulp.task('watch', function() {
    gulp.watch('app/**/*.js', ['browserify'])
    gulp.watch('sass/style.sass', ['sass'])
})

gulp.task('default', ['connect', 'watch', 'lint', 'browserify', 'compress'])