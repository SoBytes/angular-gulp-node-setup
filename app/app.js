require('angular')

var app = angular.module('app', [require('angular-route')])

app.config(function($routeProvider,$locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl : '/templates/home.html',
            controller  : 'MainController'
        })
        // route for the about page
        .when('/about', {
            templateUrl : '/templates/about.html',
            controller  : 'AboutController'
        })

        // route for the contact page
        .when('/contact', {
            templateUrl : '/templates/contact.html',
            controller  : 'ContactController'
        });
});

app.controller('MainController', function($scope) {
    $scope.message = 'Angular Works!'
})

app.controller('AboutController', function($scope) {
    $scope.message = 'AboutController Works!'
})

app.controller('ContactController', function($scope) {
    $scope.message = 'ContactController Works!'
})